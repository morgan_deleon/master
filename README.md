# Free Android Media Player (FreeAmp)

Free Android Media Player with minimalistic interface.
Based on BASS library.

**FreeAmp can:**

- Smart organization of folders by Artists, Albums, Years & Tracknumbers.
- Virtual folders for Recently Added Tracks & other files (folder Others).
- Not guzzle battery)

**Supported formats:**

- Ordinal formats (.mp3;.mp2;.mp1;.ogg;.oga;.wav;.aif;.aiff;.aifc;)
- MO3 (.mo3;.xm;.mod;.s3m;.it;.mtm;.umx;)
- Free Lossless Audio Codec (.flac;)
- MIDI (.midi;.mid;.mus;.rmi;.kar;)
- AAC (.aac;.mp4;.m4a;.m4b;*.m4p;)
- WavPack Audio Codec (.wv;.wvc;)
- Monkey's Audio (.ape;)
- MusePack (.mpc;.mpp;.mp+;)
- Apple Lossless (.mp4;.m4a;.m4b;.m4p;)
- Speex (.spx;*.wav;.oga;.ogg;)

**Contribute:**

It is an open source player and you're welcome to contribute.

**Apk file:**

[Latest stable apk file](https://bitbucket.org/recoilme/freeamp/downloads/freeamp.apk)

**Get it on Google Play:**

[Google Play](https://play.google.com/store/apps/details?id=ru.recoilme.freeamp)

**Screenshots:**

![Screenshot](https://bitbucket.org/recoilme/freeamp/raw/master/screen1.png)![Screenshot](https://bitbucket.org/recoilme/freeamp/raw/master/screen2.png)

### Author
Kulibaba Vadim <<vadim.kulibaba@gmail.com>>

### License
Distributed under [Apache 2 license](https://bitbucket.org/recoilme/freeamp/raw/master/LICENSE.txt).
