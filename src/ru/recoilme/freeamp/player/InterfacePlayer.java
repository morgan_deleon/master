package ru.recoilme.freeamp.player;

public interface InterfacePlayer {
	public void onPluginsLoaded(String plugins);
	public void onFileLoaded(String file, double duration, String artist, String title, int position, int album);
	public void onProgressChanged(double progress);
}
